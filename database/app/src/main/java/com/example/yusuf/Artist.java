package com.example.database;

public class artist {

    String artistId;
    String artistName;
    String artistGenre;

    public artist(){
    }
    public artist(String artistId, String artistName, String artistGenre){
        this.artistId = artistId;
        this.artistName = artistName;
        this.artistGenre = artistGenre;
    }
    public String getArtistId(){
        return artistId;
    }
    public String getArtistName(){
        return artistName;
    }
    public String getArtistGenre(){
        return artistGenre;
    }
}
